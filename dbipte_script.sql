USE [master]
GO
CREATE DATABASE [dbipte]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbipte', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\dbipte.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbipte_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\dbipte_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [dbipte] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbipte].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbipte] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbipte] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbipte] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbipte] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbipte] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbipte] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbipte] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbipte] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbipte] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbipte] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbipte] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbipte] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbipte] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbipte] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbipte] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbipte] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbipte] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbipte] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbipte] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbipte] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbipte] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbipte] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbipte] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbipte] SET  MULTI_USER 
GO
ALTER DATABASE [dbipte] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbipte] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbipte] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbipte] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbipte] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [dbipte] SET QUERY_STORE = OFF
GO
USE [dbipte]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[idCliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[DescProblema] [varchar](500) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([idCliente], [Correo], [Nombre], [Telefono], [DescProblema]) VALUES (1, N'a', N'a', N'a', N'a')
INSERT [dbo].[Cliente] ([idCliente], [Correo], [Nombre], [Telefono], [DescProblema]) VALUES (2, N'b', N'b', N'b', N'b')
INSERT [dbo].[Cliente] ([idCliente], [Correo], [Nombre], [Telefono], [DescProblema]) VALUES (3, N'c', N'c', N'c',N'c')
SET IDENTITY_INSERT [dbo].[Cliente] OFF
GO
/****** Object:  StoredProcedure [dbo].[Actualizar_Cliente]    Script Date: 19/05/2024 04:23:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Actualizar_Cliente]
(
	@idCliente int,
	@nombre nvarchar(50),
	@correo nvarchar(50),
	@telefono nvarchar(50),
	@descProblema nvarchar(500)
)
as
begin
	 Update Cliente
    set Nombre = @nombre
      , Correo = @correo
      , Telefono = @telefono 
	  , DescProblema = @descProblema
    Where Cliente.idCliente = @idCliente 
end
GO
/****** Object:  StoredProcedure [dbo].[Guardar_Cliente]    Script Date: 19/05/2024 04:23:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ernesto Mendoza>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create procedure [dbo].[Guardar_Cliente]
       @Nombre                     NVARCHAR(50)  = NULL   , 
       @Correo                  NVARCHAR(50)      = NULL   , 
       @Telefono                       NVARCHAR(50)  = NULL   , 
       @DescProblema             NVARCHAR(500)  = NULL  
AS
BEGIN
     SET NOCOUNT ON 

     INSERT INTO dbo.Cliente
          (                    
            Nombre                     ,
            Correo                  ,
            Telefono                      ,
            DescProblema                 
          ) 
     VALUES 
          ( 
            @Nombre,
            @Correo,
            @Telefono,
            @DescProblema
          ) 

END
GO
/****** Object:  StoredProcedure [dbo].[Listar_ClientePorId]    Script Date: 19/05/2024 04:23:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Listar_ClientePorId]
(
	@idCliente int
)
as
begin
	select *
	from Cliente
	where idCliente = @idCliente
end
GO
/****** Object:  StoredProcedure [dbo].[Listar_Clientes]    Script Date: 19/05/2024 04:23:44 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Listar_Clientes]
as
begin
	select *
	from Cliente
end
GO